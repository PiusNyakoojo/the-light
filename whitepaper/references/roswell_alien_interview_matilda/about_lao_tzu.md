
## About Lao Tzu

Recording: Roswell Alien Interview
Start Time: 2:35:35
End Time: 2:37:40

604 BCE.

Lao Zi a philosopher who wrote a small book called "The Way" was an is-be of great wisdom who overcame the effects of the old empire amnesia hypnosis machinery and escaped from Earth.

His understanding of the nature of an is-be must have been very good to accomplish this. According to the common legend his last life time as a human was lived in a small village in China. He contemplated the existence of his own life. Like Guatama Siddhartha he confronted his own thoughts and past lives and doing so he recovered some of his own memory, ability and immortality.

As an old man he decided to leave the village and go to the forest to depart the body. The village gatekeeper stopped him and begged him to write down his personal philosophy before leaving.

Here is a small piece of the advice he gave about "The Way". He re-discovered his own spirit:

```
He who looks will not see it. He who listens will not hear it. He who gropes will not grasp it. The formless non-entity. The motionless source of motion. The infinite essence of the spirit is the source of life. Spirit is self.

Walls form and support a room yet the space between them is most important. A pot is formed of clay yet the space formed therein is most useful. Action is caused by the force of nothing on something. Just as the nothing of spirit is the source of all form. One suffers great afflictions because one has a body. Without a body, what afflictions could one suffer?

When one cares more for the body than for his own spirit one becomes the body and loses the way of the spirit. The self, the spirit, creates illusion. The delusion of man is that reality is not an illusion. One who creates illusions and makes them more real than reality follows the path of the spirit and finds the way of heaven.
```
