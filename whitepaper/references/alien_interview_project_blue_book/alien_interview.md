
Alien Interview - Project Blue Book
[Part 1 - https://www.youtube.com/watch?v=G2xXu8_2Exo]
[Part 2 - https://www.youtube.com/watch?v=7TE6frpygVY]


PART 1 TRANSCRIPT
Meaning lives in the mind. Humans conjure meaning. Meaning is something that is ascribed. Nature is the objective reality.


PART 2 TRANSCRIPT

Responder:
Nothing, by definition, does not exist. Since nothing cannot exist, what is left is existence.
Existence is infinite. It has no end. No beginning and therefore no creator.
This universe is not existence. It is an infinitely small part of existence.
This universe is a spontaneous event and inevitable within the eternity of existence.
Every event can, will, and has happened. Including this universe.
There are an infinite number of universes. Virtually all cannot harbor so-called life.
This universe is, by chance, stable. And has the occasional capacity to harbor so-called life.
Life, as we call it, is an inevitable consequence of this universe's physical properties.

Interviewer: So are you telling me we're just random?
Responder: Yes. This universe is indifferent to so-called life.
Interviewer: How so?
Responder: Life on this and every world can be destroyed at any time by a multitude of random events.
Interviewer: Such as?
Responder: What you would term supernova... solar flare... asteroid impact.
Interviewer: So the Universe doesn't care if we live or die?
Responder: Correct.
Interviewer: I don't believe that for a second. We're more than just random.
Responder: As I said, you are not capable of accepting the truths of existence.
Interviewer: So if we're just 'random'... then there's no meaning to the Universe.
Responder: That is correct.
Interviewer: So if there's no meaning in the Universe... then what's the point of living?
Responder: There is meaning.
Interviewer: You just contradicted yourself again.
Responder: Meaning lives in the mind.
Interviewer: No, no no.. You can't live a full life.. You can't live a life at all if you think that meaning is somehow made up.
Responder: Your species [Homo sapiens] conjures meaning... but operates under the false belief that meaning is a mystical plan. It is not.
Interviewer: So what is meaning?
Responder: Meaning is what you make it.
Interviewer: Okay, moving on.

Part 1 Video Source: https://www.youtube.com/watch?v=G2xXu8_2Exo
Part 2 Video Source: https://www.youtube.com/watch?v=7TE6frpygVY

## Personal Research Notes
