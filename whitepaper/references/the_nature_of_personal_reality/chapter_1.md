Consciousness is within all physical phenomenon.

The personal life that you know rises up from within you. You give yourself the life that is lived through you. You make your own reality. There is no other rule. Knowing this is the secret to creativity.

The ego is only a portion of you. It is that expert part of your personality that deals directly with the contents of your conscious mind. And is concerned most directly with the material portions of your experience. The ego is a very specialized portion of your greater identity. It is a portion of you that arises to deal directly with the life that the larger you is living.

The ego and the conscious mind are not the same thing. The ego is composed of various portions of the personality. It is a combination of characteristics, ever changing, that act in a unitary fashion. The portion of the personality that deals most directly with the world.

```
The conscious mind is an excellent perceiving attribute, a function that belongs to inner awareness, but in this case is turned outward toward the world of events.
```

Consciousness isn't a thing. It's a quality.

## Personal Research Notes
