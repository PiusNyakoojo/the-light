
## Question:
Is there a scientific substantiation for paranormal powers, as for example with your powers of thought?

## Answer:
Yes. In order to explain that, one has to acknowledge the physical reality of the sphere of influence {Feldraum}.

You are going to have to separate yourself mentally from the illusion that that which you see is the true nature of the universe.

It is, at best, the surface of a side. Imagine for yourself that all the matter here - you, this table, this pencil, this technical device, this paper - does not really exist, but that it is rather only the result of a field oscillation and a concentration of energy.

All matter that you see, every creature, every planet and star in this universe, has an "information-energy equivalent" in the sphere of influence which is located on a main field - the general level {of things}.

Now, there is not only one level but several.

--

Existence is always a duality. Some layers of the field contain simple information about the solid matter of your body and its frequency, while other layers {contain information about} your spirit, your consciousness or, speaking from a human-religious point of view, your soul.

```
Awareness or consciousness in this case is a simple energy matrix, divided into different layers of your field in the sphere of influence - nothing more, nothing less.
```

Genuine awareness can also exist here on the matter side, but only in the form of post-plasma {the fifth form of matter}.

```
With the necessary physical knowledge and the corresponding technology, the consciousness/awareness matrix, or soul, can also be separated from its field of rest.

It can, despite its removal, continue to exist in a self-sufficient manner for a certain amount of time.
```




## Personal Research Notes

An `energy matrix`, or power matrix, is a term used to describe the primary source of energy for a system. This system can be either organic or technological, and the matrix is responsible for the distribution of energy throughout.
