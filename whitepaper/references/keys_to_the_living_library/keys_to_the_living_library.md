
[page 8 - Earth Pleiadian Keys to the Living Library]
We intend to implant new images in your mind to take you further. It doesn't matter how we do it. It doesn't matter if it is true. It simply matters that we create new images for you.

[page 9 - Earth Pleiadian Keys to the Living Library]
It is time for you to make a commitment to create joy, creativity, and love for yourself. Only then will you benefit others, for if you do not evolve yourself, you do not serve others.

By becoming a living example, by following what is in your heart, you show the way for others to follow with courage what is in their hearts.

[page 14 - Earth Pleiadian Keys to the Living Library]
The codes of consciousness contain the songs of your own freedom, sung as frequency and broadcast from the cells of your body.

[page 20 - Earth Pleiadian Keys to the Living Library]
The word 'Maya' refers to the illusion of reality.
