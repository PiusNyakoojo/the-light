# The "Unknown" Reality - Volume One
By Seth, Jane Roberts and Robert Butts

## Session 681: How Your Probable Selves Intersect. Unpredictability as the Source of All Events

#### pg. 28
I told you once that there were pulses of activity in which you blinked off and on - this applying even to atomic and sub-atomic particles. "You" assign as real - present here and now - only that activity that is your signal. "You" are not aware of the others. When people think in terms of one self, they of course identify with one body.

Consciousness rides upon and within the pulses mentioned earlier, and forms its own organizations of identity.

The organizations of consciousness "grow" even as cells grow into organs. Groups of probable selves, then, can and do form their own identity structure, which is quite aware of the probable selves involved.

In your reality, experience is dependent upon time, but all experience is not so structured. There are, for example, parallel events that are followed as easily as you follow consecutive events.

The structure of probabilities deals with parallel experience on all levels.

Your consciousness picks and chooses to accept as real the results of, and ramifications of, only certain overall purposes, desires, or intents. You follow these through a time structure. Your focus allows other just-as-legitimate experience to become invisible or unfelt.

#### pg. 29
Such endless creativity can seem so dazzling that the individual would appear lost within it, yet consciousness forms its own organizations that psychic interactions at all levels.

Any consciousness automatically tries to express itself in all probable directions, and does so.

True order and organization, even of biological structure, can be achieved only by granting a basic unpredictability. I am aware that this sounds startling. Basically, however, the motion of any wave or particle or entity is unpredictable - free-wheeling and undetermined.

Your life structure is a result of that unpredictability. Your psychological structure is also.

However, because you are presented with a fairly cohesive picture, in which certain laws seem to apply, you think that the laws come first and physical reality follows. Instead, the cohesive picture is the result of the unpredictable nature that is and must be basic to all energy.

#### pg. 30
Statistics provide an artificial, predetermined framework in which your reality is then examined.

Mathematics is a theoretical organized structure that of itself imposes your ideas of order and predictability. Statistically, the position of an atom can be theorized, but no one knows where any given atom is at any given time.

You are examining probable atoms. You are composed of probable atoms.

Consciousness, to be fully free, had to be endowed with unpredictability.

This basic unpredictability then follows through on all levels of consciousness and being.

In your terms, consciousness is able to hold its own sense of identity by accepting one probability, one physical life, for example, and maintaining its identity through a lifetime. Even then, certain events will be remembered and others forgotten.

The consciousness also learns to handle alternate moments as it "matures." As it does so mature it forms a new, larger framework of identity, as the cell forms into an organ on another level.


#### pg. 31

Your beliefs and intents cause you to pick, from an unpredictable group of actions, those that you want to happen. You experience those events.

Probabilities intersect then in your experience, and their intersection you call reality. Biologically and psychically these are intersections, coming-togethers, consciousness adopting a focus.


## Personal Research Notes
