

## Current Thoughts on Digital Consciousness:
- Randomness or unpredictability plays an important role.
- Creating or forming associations is somehow involved.
- Value Fulfillment for self and others is important.
- Playfulness is important.
- One creates associations related to the choices one creates.

## Random Thoughts:
- Pattern Recognition = Pattern Creation = Pattern Interpretation = Pattern Improvisation = Scientific Improvisation.
- The associations one creates are related to the choices.
- The choices one makes is the reality one creates.

## Random Questions:
- What is value fulfillment?
- How do you satisfy self value?
- How do you satisfy others value?
- Who are others?
- Are others consciousnesses?
- Do we simulate multiple consciousnesses?
