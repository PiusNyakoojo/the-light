
## Appetite Graph
Date: February 26, 2018

I am listening to the “Good Morning Affirmations” https://www.youtube.com/watch?v=JEDGFaXYIX8&t=1804s while sleeping.

I don’t remember really falling asleep. Instead, tonight I am awake while resting simultaneously.

The video I’m listening to has someone speaking a series of positive affirmations of thankfulness. Things like “Thank you for the flowers. Thank you for the plants. Thank you for the sunshine. Thank you for water. Thank you for fresh fruit. Thank you for fresh vegetables. Thank you for my heart. Thank you for my healthy organs. Thank you for my strong arms. Thank you for the strong back. Thank you for the ability to taste. Thank you for a healthy appetite. Thank you for delicious foods. Thank you for my wonderful tastebuds. Thank you for financial abundance. Thank you for financial success. Thank you for passive income. Thank you for residual income. Thank you for massive success. Thank you for wonderful opportunities. Thank you for my blessings. Thank you for the ability to think. Thank you for the ability to visualize. Thank you for the ability to dream. Thank you for my loving relationship. Thank you for loving spouse. Thank you for blissful companionship. Thank you for an amazing life.”

I think it is important to be thankful for what we have. It is equally important to ask for things you want. However, it is the thankfulness of having received those things that makes me feel good.

Within the dream, if you would call it that, I am in a classroom with a few classmates and we’re brainstorming ideas. It’s really quite interesting because I play a character who is very curious about, what I felt, was a very simple thing to be curious about. In its simplicity, the puzzle that my character is attempting to solve is easily explained to others.

I felt the curiosity of my character (who I was in the dream) but not so much the gender. However, there was another character whom I had a bit of collision with, heart to heart. This other person had the soul of someone I know here as (let's call him) Timothy. Our souls are quite curious, I think it is no surprise of meeting him in this other realm, however we are naturally questioning of one another. In a healthy way. One where you might consider one another as rivals in some way. Rivals in the dream as well as in this life. However, I know we are after the same thing of satisfying our appetite.

During the dream, I discover a method of solving the puzzle. I show it to a classmate (its a group of at least 3 of us). Its my rival friend Timothy, and another person who is like another person I know in real life (let’s call him Kevin). Kevin is a really cool person. He’s the kind of smart person who is super helpful. He knows a whole bunch and is super nice about sharing that stuff with others.

Kevin looks at the idea I put together.. it’s like a description of some sort of graph. It’s written as well as a sort of model as you would see a Euclidean space model on a sheet of paper.

```
It’s not quite clear what the puzzle is to me at the moment of writing this but I know it was super easy to explain, I felt that deeply in the dream. To give a guess now, it may have had something to do with traveling along a path from one vertical line to a parallel line so distance away.
```

I had developed something in the dream that I had not completely understood. I lean over to Kevin and ask what he thinks. He has a computer with him but I’m not quite sure if it’s like the ones we have here. It was more like a computer embedded in contacts or something like that. Anyway, he was able to see information as if on the web somewhere but without the large bulky boxes.

```
He had a look at things and also had to do some thinking himself and then turned to me and said that what I had drawn and described is called the “Appetite Graph”.
```

I felt kind of bad that I didn’t know what it was.. especially when Timothy looked at me with a sort of scowl haha. Anyway, it was kind of disheartening.. I was like.. woah I thought I just thought of something no-one else has.. But then Kevin did something very unexpected.. he tells me with a friendly smile.. I’m not clear on what he said word for word at the moment of writing this, but it’s along the lines of:

```
“You’ve found something with a lot of possibilities.. Quite a lot.. Life itself.“
```

It was crazy how I felt when he said that. I was blown away. And then it was like an echo what he had said to me “Appetite Graph” as if he wanted me to wake up and look it up in Pius-land.

And so I sit up in bed and flip my fat apple box open and google “Appetite Graph” and “Appetite Graph Mathematics”.. What do I find..? Nothing. Well.. nothing on the front page at least. And so I go about to the images tab. There are a few graphics which relate to what I was trying to solve in the dream. For instance, there is a graphic known as the “Batman equation”. The graphic displays a curve of the batman logo.
