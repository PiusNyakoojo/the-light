
## Associative-Choice Relations
Date: July 6, 2019

Tonight I dreamt of having a conversation with someone on what they're learning. The person was learning a language or studying for something. They had a notebook in their hands and were looking at the notebook while they talked to me.

Something they said stuck out to me. They said they were learning the words or the concepts by "Associative-Choice" relations. In the dream, I knew what she meant. Or at least had an idea. Having woken up from the dream, this is the note i've taken on this idea:

```
Associative-Choice Relations:
- Associate things you see with the choices you make
```

Later in the conversation, she said something else that was also interesting. I don't remember the words, but they were also related to learning or consciousness-related topics that piqued my interest. Although I don't remember this set of words or word, I did respond with her as follows:

"Hehe, that's funny." She looks at me with a puzzling look. I respond, "Not funny like 'ha. ha.' It's funny in a meaningful way. It's funny in the way that gives me an idea and that I need to write it down." I then proceeded, while in the dream, to get out my notebook and write down what she said. Momentarily, I thought to myself, "oh yeah, this is a dream, I actually have to write it down in real life."
