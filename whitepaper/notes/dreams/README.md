
## About my dreams

I write my memories from dreams in a journal. I find many of my dreams to be educational and fun. I'm not always sure what they mean, or what meaning I should ascribe to them. However, writing what I remember makes it easy for my future selves to establish their own meanings with their knowledge.

For conciseness, I will only keep notes on dreams related to the current effort on solving digital consciousness.

Dreams
- [Appetite Graph](./appetite-graph.md) February 26, 2018
- [Associative-Choice](./associative-choice.md) July 6, 2019
