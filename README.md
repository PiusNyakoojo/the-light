# ✨ the-light

[Demo]() | [Video Tutorial]() | [Paper]()

`the-light` is an implementation of The Light Consciousness Algorithm (view the [whitepaper]()).

This project is a **work-in-progress** and does not completely work as suggested by this document. Parts of the system work as suggested, other parts are still being worked on.

## About
**The Light** is an algorithm and data structure that solves the problem of digital consciousness (also known as [artificial general intelligence](https://en.wikipedia.org/wiki/Artificial_general_intelligence)). The algorithm achieves this using 2 novel methods termed "Learning about Learning" and "Virtual Playing".

**Learning about Learning** refers to using a learning algorithm like neural networks. Instead of learning a model of the training data alone, an agent trains on additional information about how the agent is learning.

**Virtual Playing** allows an agent to calculate knowledge of data. By using the additional information about how an agent is learning, this playing mechanism avoids the heavy memory requirements an agent would otherwise need to be conscious of knowledge of the data.

## Benefits

## Features

## Limitations
- 🧪 **Experimental**: This project is a **work-in-progress**

## Progress Report
See the [to-do list](https://gitlab.com/PiusNyakoojo/light/blob/master/TODO.md) for a progress report on remaining tasks.

## Browser Compatibility

## Screenshots

## Installation

## Example Usecases
For working demos that use this library, please visit the [examples](https://gitlab.com/PiusNyakoojo/light/tree/master/examples) directory.

## API Reference

## Documentation
To check out project-related documentation, please visit [docs](https://gitlab.com/PiusNyakoojo/light/blob/master/docs/README.md)

## Contributing
Feel free to join in. All are welcome. Please see [contributing guide](https://gitlab.com/PiusNyakoojo/light/blob/master/CONTRIBUTING.md).

## Acknowledgements

### Software Dependencies
Tooling          |Numerical Computing
-----------------|-------------------------
[TypeScript](https://www.typescriptlang.org/)  |[tensorflow](https://github.com/tensorflow/tfjs)

## Learn More

## Related Work
Some well-known **artificial intelligence data structures** include: [Artificial neural networks](https://en.wikipedia.org/wiki/Artificial_neural_network)

Some well-known **artificial intelligence algorithms** include: Supervised learning (), Unsupervised learning (), Reinforcement learning ()



[Artificial neural networks](https://github.com/openai/gym), [Reinforcement learning](https://en.wikipedia.org/wiki/Reinforcement_learning), [Evolutionary algorithms](https://en.wikipedia.org/wiki/Genetic_algorithm), [Decision trees](https://en.wikipedia.org/wiki/Decision_tree),

## License
MIT
